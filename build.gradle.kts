import Build_gradle.Version.Companion.checker
import Build_gradle.Version.Companion.log4j
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import Build_gradle.Version.Companion.spring
import sun.net.ExtendedOptionsImpl

group = "org.sparrow.shoppingmanager"
version = "1.0"

class Version {
  companion object {
    const val spring = "5.0.3.RELEASE"
    const val servlet = "4.0.0"
    const val jsp = "2.3.2-b02"
    const val jstl = "1.2"
    const val checker = "2.5.0"
    const val klog = "1.4.9"
    const val log4j = "2.10.0"
    const val slf4j = "1.7.25"
    const val fasterxml = "2.9.4"
  }
}

buildscript {
  repositories {
    mavenCentral()
    maven { setUrl("https://repo.spring.io/plugins-snapshot") }
  }
  dependencies {
    classpath("io.spring.gradle:spring-build-conventions:0.0.14.RELEASE")
    classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.2.20")
    classpath("org.jetbrains.kotlin:kotlin-allopen:1.2.20")
  }
}

plugins {
  val kotlinVersion = "1.2.21"
  id("org.jetbrains.kotlin.jvm") version kotlinVersion
  id("org.jetbrains.kotlin.plugin.spring") version kotlinVersion
  id("org.jetbrains.kotlin.plugin.jpa") version kotlinVersion
  id("io.spring.dependency-management") version "1.0.4.RELEASE"
}
apply {
  plugin("io.spring.convention.spring-sample-war")
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    jvmTarget = "1.8"
    freeCompilerArgs = listOf("-Xjsr305=strict")
  }
}

val test by tasks.getting(Test::class) {
  useJUnitPlatform()
}

repositories { mavenCentral() }

dependencies {
  fun spring(module: String, version: String = Version.spring): Any =
      "org.springframework:spring-$module:$version"

  fun security(module: String, version: String = Version.spring) =
      "org.springframework.security:spring-security-$module:$version"

  fun log4j(module: String, version: String = Version.log4j) =
      "org.apache.logging.log4j:log4j-$module:$version"

  fun fasterxml(subGroup: String, name: String, version: String = Version.fasterxml) =
      "com.fasterxml.jackson.$subGroup:jackson-$name:$version"

  implementation(kotlin("stdlib-jdk8", "1.2.31"))
  implementation(kotlin("reflect"))

  implementation(spring("core"))
  implementation(spring("context"))
  implementation(spring("webmvc"))
  implementation(security("web"))
  implementation(security("config"))
  implementation("javax.servlet", "jstl", Version.jstl)
  compileOnly("javax.servlet", "javax.servlet-api", Version.servlet)
  compileOnly("javax.servlet.jsp", "javax.servlet.jsp-api", Version.jsp)

  implementation("io.github.microutils", "kotlin-logging", Version.klog)
  implementation(log4j("core"))
  implementation(log4j("api"))
  implementation(log4j("slf4j-impl"))
  implementation(fasterxml("core", "core"))
  implementation(fasterxml("core", "databind"))
  implementation(fasterxml("dataformat", "dataformat-yaml"))
}

configure<JavaPluginConvention> {
  sourceCompatibility = JavaVersion.VERSION_1_8
  targetCompatibility = JavaVersion.VERSION_1_8
}
