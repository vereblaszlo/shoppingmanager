package org.sparrow.shoppingmanager.controller

import mu.KotlinLogging
import org.sparrow.shoppingmanager.service.MessageService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView

private val LOG = KotlinLogging.logger {}

/**
 * The main entry point for visitors.
 */
@Controller
class MainController internal constructor(private val messageService: MessageService) {

  /**
   * View administration page.
   *
   * @return administration model
   */
  @GetMapping(ADMIN_PATH)
  fun visitAdmin(): ModelAndView {
    val model = ModelAndView(ADMIN_VIEW)
    model.addObject(TITLE_KEY, messageService.getMessage(MAIN_CONTROLLER_TITLE))
    model.addObject(MESSAGE_KEY, messageService.getMessage(MAIN_CONTROLLER_MESSAGE))
    return model
  }

  /**
   * View home page.
   *
   * @return home model
   */
  @GetMapping(HOME_PATH)
  fun visitHome(): ModelAndView {
    LOG.trace("This is a trace message!!!")
    LOG.debug("This is a  debug message!!!")
    LOG.info("This is an info message!!!")
    LOG.warn("This is a warn message!!!")
    LOG.error("This is an error message!!!")
    val model = ModelAndView(HOME_VIEW)
    model.addObject(TITLE_KEY, messageService.getMessage(MAIN_CONTROLLER_TITLE))
    model.addObject(MESSAGE_KEY, messageService.getMessage(MAIN_CONTROLLER_MESSAGE))
    return model
  }

  companion object {
    private const val ADMIN_PATH = "/admin"
    private const val HOME_PATH = "/"

    private const val MAIN_CONTROLLER_MESSAGE = "MainController.message"
    private const val MAIN_CONTROLLER_TITLE = "MainController.title"

    private const val ADMIN_VIEW = "admin"
    private const val HOME_VIEW = "home"

    private const val MESSAGE_KEY = "message"
    private const val TITLE_KEY = "title"
  }
}
