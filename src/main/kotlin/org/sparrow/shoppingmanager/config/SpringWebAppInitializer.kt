package org.sparrow.shoppingmanager.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer

/**
 * Initialiser for mvc.
 */
@Configuration
class ApplicationWebInitializer : AbstractAnnotationConfigDispatcherServletInitializer() {
  override fun getServletConfigClasses(): Array<Class<*>> = arrayOf(MvcConfig::class.java)

  override fun getServletMappings(): Array<String> = arrayOf(ROOT)

  override fun getRootConfigClasses(): Array<Class<*>>? = null

  companion object {
    private const val ROOT = "/"
  }
}
