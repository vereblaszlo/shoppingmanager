package org.sparrow.shoppingmanager.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer

/**
 * Initialiser for security.
 */
@Configuration
class SecurityWebApplicationInitializer : AbstractSecurityWebApplicationInitializer(SecurityConfig::class.java)
