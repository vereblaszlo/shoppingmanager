package org.sparrow.shoppingmanager.config

import org.sparrow.shoppingmanager.dao.MessageDao
import org.sparrow.shoppingmanager.service.MessageService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
internal class ServiceConfig {
  @Bean
  fun messageService(messageDao: MessageDao) = MessageService(messageDao)
}
