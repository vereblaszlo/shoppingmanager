package org.sparrow.shoppingmanager.config

import org.sparrow.shoppingmanager.dao.MessageDao
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
internal class DaoConfig {
  @Bean
  fun messageDao() = MessageDao("controller_messages")
}
