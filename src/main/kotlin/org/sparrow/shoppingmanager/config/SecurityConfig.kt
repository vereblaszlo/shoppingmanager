package org.sparrow.shoppingmanager.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

/**
 * Configuration file for security.
 */
@Configuration
@EnableWebSecurity
class SecurityConfig : WebSecurityConfigurerAdapter() {

  /**
   * Configures http security.
   */
  override fun configure(http: HttpSecurity) {
    http.authorizeRequests()
        .antMatchers(PATH_ROOT).permitAll()
        .antMatchers(PATH_ADMIN).access(has(ROLE_ADMIN)).and().formLogin()
    http.csrf().disable()
  }

  /**
   * Configures authentication.
   */
  @Autowired
  override fun configure(auth: AuthenticationManagerBuilder) {
    auth.inMemoryAuthentication()
        .withUser(ADMIN_USER_NAME).password(ADMIN_USER_PSWD).roles(ROLE_ADMIN)
  }

  companion object {
    private const val PATH_ADMIN = "/admin**"
    private const val PATH_ROOT = "/"

    private const val ADMIN_USER_PSWD = "nimda"
    private const val ADMIN_USER_NAME = "admin"

    private const val ROLE_ADMIN = "ADMIN"
  }

  private fun has(roleAdmin: String) = "hasRole('$roleAdmin')"
}
