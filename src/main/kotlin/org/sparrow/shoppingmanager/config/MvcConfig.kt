package org.sparrow.shoppingmanager.config

import org.sparrow.shoppingmanager.config.MvcConfig.Companion.PATH_CONFIG
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.view.InternalResourceViewResolver

/**
 * The configuration file for mvc.
 */
@EnableWebMvc
@Configuration
@ComponentScan(PATH_CONFIG)
@Import(ControllerConfig::class, ServiceConfig::class, DaoConfig::class)
class MvcConfig {
  /**
   * Initialises view resolver.
   *
   * @return view resolver
   */
  @Bean
  fun viewResolver() = InternalResourceViewResolver().apply { setPrefix(PREFIX) }.apply { setSuffix(SUFFIX) }

  companion object {
    internal const val PATH_CONFIG = "org.sparrow.shoppingmanager.config"

    private const val SUFFIX = ".jsp"
    private const val PREFIX = "/WEB-INF/views/"
  }
}
