package org.sparrow.shoppingmanager.config

import org.sparrow.shoppingmanager.controller.MainController
import org.sparrow.shoppingmanager.service.MessageService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Configurations for beans.
 */
@Configuration
internal class ControllerConfig {
  @Bean
  fun mainController(messageService: MessageService) = MainController(messageService)
}
