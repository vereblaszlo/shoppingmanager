package org.sparrow.shoppingmanager.service

import org.sparrow.shoppingmanager.dao.MessageDao

/**
 * Messaging service.
 */
class MessageService(private val messageDao: MessageDao) {
  /**
   * Returns the message associated with the key.
   */
  fun getMessage(key: String) = messageDao.getMessage(key)
}
