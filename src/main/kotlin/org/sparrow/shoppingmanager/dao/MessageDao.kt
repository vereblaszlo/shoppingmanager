package org.sparrow.shoppingmanager.dao

import mu.KotlinLogging
import java.util.*

private val LOG = KotlinLogging.logger {}

/**
 * Message internalisation.
 */
class MessageDao(bundleName: String) {

  private val resourceBundle: ResourceBundle = ResourceBundle.getBundle(bundleName)

  /**
   * Returns the internationalised message associated with the key.
   *
   * @param key of the message
   * @return the associated message in session language
   */
  fun getMessage(key: String): String {
    var message: String
    try {
      message = resourceBundle.getString(key)
      if (message == null) {
        LOG.error(
            getErrorMessage(key))
        message = '!'.toString() + key + '!'.toString()
      }
    } catch (exc: MissingResourceException) {
      LOG.error(
          getErrorMessage(key), exc)
      message = '!'.toString() + key + '!'.toString()
    } catch (exc: NullPointerException) {
      LOG.error(
          getErrorMessage(key), exc)
      message = '!'.toString() + key + '!'.toString()
    }

    return message
  }

  companion object {
    private fun getErrorMessage(key: String): String = "Resource key '$key' is missing"
  }
}
